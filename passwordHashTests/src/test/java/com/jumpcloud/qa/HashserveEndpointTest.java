package com.jumpcloud.qa;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;

import retrofit2.Response;
import java.util.stream.IntStream;

import com.jumpcloud.qa.common.adapters.RetrofitAdapter;
import com.jumpcloud.qa.common.adapters.RetrofitAdapter.Callback;
import com.jumpcloud.qa.common.interfaces.HashserveInterface;
import com.jumpcloud.qa.common.models.HashPasswordBody;
import com.jumpcloud.qa.common.models.HashStatsResponse;
import com.jumpcloud.qa.common.helpers.HashHelper;

@Test(singleThreaded = true)
public class HashserveEndpointTest {
	private static String baseUrl;
	
	@BeforeClass(alwaysRun = true)
	public void setup_tests() {
		String prop_url = System.getProperty("serviceHostUrl");
		if (prop_url==null) prop_url = "http://127.0.0.1";
		
		String prop_port = System.getProperty("servicePort");
		if (prop_port==null) prop_port = "8088";
		
		this.baseUrl = prop_url + ":" + prop_port;
	}
	
	@Test
	public void test_PostAndGetSuccessfullyReturnsCorrectHash() {
		String password = "thisIsATestPassword";
		HashserveInterface iHash = RetrofitAdapter.fromInterface(HashserveInterface.class, baseUrl);

		Response<Integer> postResponse = RetrofitAdapter.executeCall(iHash.postPassword(new HashPasswordBody(password)));
		Assert.assertEquals(postResponse.code(), 200, String.format("Unexpected status code returned by POST /hash: [%d]\n", postResponse.code()));

		Response<String> getResponse = RetrofitAdapter.executeCall(iHash.getHash(postResponse.body()));
		Assert.assertEquals(getResponse.code(), 200, String.format("Unexpected status code returned by GET /hash: [%d]\n", getResponse.code()));

		Assert.assertFalse(getResponse.body().isEmpty(), "GET /hash returned an empty body.\n");

		Assert.assertEquals(getResponse.body(), HashHelper.generateHash(password), "Server returned unexpected hash.\n");
	}
	
	@Test
	public void test_ParallelPostHashSuccessful() {
		HashserveInterface iHash = RetrofitAdapter.fromInterface(HashserveInterface.class, baseUrl);

		Response<Integer> postResponse = RetrofitAdapter.executeCall(iHash.postPassword(new HashPasswordBody("checkingTheNextIndex")));
		Assert.assertEquals(postResponse.code(), 200, String.format("Unexpected status code returned by POST /hash: [%d]\n", postResponse.code()));
		int currentIndex = postResponse.body().intValue();

		IntStream.range(1,10).parallel().forEach(i -> {
			String password = "thisIsATestPassword" + i;
			Response<Integer> parallelPostResponse = RetrofitAdapter.executeCall(iHash.postPassword(new HashPasswordBody(password)));
			Assert.assertEquals(parallelPostResponse.code(), 200, String.format("Unexpected status code returned by POST /hash: [%d]\n", parallelPostResponse.code()));
		});

		postResponse = RetrofitAdapter.executeCall(iHash.postPassword(new HashPasswordBody("finalTestPassword")));
		Assert.assertEquals(postResponse.code(), 200, String.format("Unexpected status code returned by POST /hash: [%d]\n", postResponse.code()));
		Assert.assertEquals(postResponse.body().intValue(), currentIndex+10, "Unexpected index returned after parallel hashing calls.\n");
	}
	
	@Test
	public void test_PostHashAsyncPerformance() {
		HashserveInterface iHash = RetrofitAdapter.fromInterfaceWithTimeout(HashserveInterface.class, baseUrl, 500);
		
		try {
			Response<Integer> postResponse = RetrofitAdapter.executeCall(iHash.postPassword(new HashPasswordBody("shouldBeAsync")));
		} catch (Exception ex) {
			Assert.fail("Exception when trying to POST to /hash with 0.5 second timeout.\n" + ex.getMessage() + "\n");
		}
	}
	
	@Test
	public void test_StatsIncrementAndReturnSuccessfully() {
		HashserveInterface iHash = RetrofitAdapter.fromInterface(HashserveInterface.class, baseUrl);
		
		Response<HashStatsResponse> statsResponse = RetrofitAdapter.executeCall(iHash.getStats());
		Assert.assertEquals(statsResponse.code(), 200, String.format("Unexpected status code returned by GET /stats: [%d]\n", statsResponse.code()));
		int requestCount = statsResponse.body().getRequestCount();
		
		RetrofitAdapter.executeCall(iHash.postPassword(new HashPasswordBody("letsBumpTheIndex")));
		
		statsResponse = RetrofitAdapter.executeCall(iHash.getStats());
		Assert.assertEquals(statsResponse.body().getRequestCount(), requestCount + 1, "Unexpected request count returned by server after making one additional request.\n");
	}
	
	@Test(priority=3)
	public void test_GracefulShutdownAndDeny() {
		HashserveInterface iHash = RetrofitAdapter.fromInterface(HashserveInterface.class, baseUrl);
		
		Callback<Integer> postResponse = new Callback<>();
		iHash.postPassword(new HashPasswordBody("inBeforeTheDeadline")).enqueue(postResponse);
		
		Response<Void> shutdownResponse = RetrofitAdapter.executeCall(iHash.shutdown("shutdown"));
		Assert.assertTrue(shutdownResponse.isSuccessful(), String.format("Shutdown request returned an unsuccessful response: [%d]", shutdownResponse.code()));
		
		Assert.assertTrue(postResponse.wasSuccessful(), "Hash request sent before the shutdown did not return successfully.");
	}
}