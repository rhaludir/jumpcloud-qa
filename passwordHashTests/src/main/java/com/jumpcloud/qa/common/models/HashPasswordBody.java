package com.jumpcloud.qa.common.models;

import com.fasterxml.jackson.annotation.*;

public class HashPasswordBody {
	@JsonProperty("password")
	private String password;
	
	public HashPasswordBody() {
		this("");
	}
	
	public HashPasswordBody(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}