package com.jumpcloud.qa.common.models;

import com.fasterxml.jackson.annotation.*;

public class HashStatsResponse {
	@JsonProperty("TotalRequests")
	private int requestCount;
	@JsonProperty("AverageTime")
	private int avgTimeMills;
	
	public int getRequestCount() {
		return this.requestCount;
	}
	
	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}
	
	public int getAvgTimeMills() {
		return this.avgTimeMills;
	}
	
	public void setAvgTimeMills(int avgTimeMills) {
		this.avgTimeMills = avgTimeMills;
	}
}