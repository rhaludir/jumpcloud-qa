package com.jumpcloud.qa.common.helpers;

import java.math.BigInteger;
import java.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashHelper {
	
	public static String generateHash(String password) {
		String result = null;
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] digest = md.digest(password.getBytes());
			
			//BigInteger signum = new BigInteger(1, digest);
			String hexHash = Base64.getEncoder().encodeToString(digest);
			
			return hexHash;
		}
		catch (NoSuchAlgorithmException ex) {
			return null;
		}
	}
}