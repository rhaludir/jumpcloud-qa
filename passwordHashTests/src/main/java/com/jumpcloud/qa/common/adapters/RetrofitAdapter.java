package com.jumpcloud.qa.common.adapters;

import java.lang.Class;
import java.lang.Throwable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class RetrofitAdapter {
	
	public static <T> T fromInterface(Class<T> clazz, String baseURL) {
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(baseURL)
			.addConverterFactory(ScalarsConverterFactory.create())
			.addConverterFactory(JacksonConverterFactory.create())
			.build();
		
		return retrofit.create(clazz);
	}
	
	public static <T> T fromInterfaceWithTimeout(Class<T> clazz, String baseURL, int timeoutMills) {
		final OkHttpClient client = new OkHttpClient.Builder()
			.writeTimeout(timeoutMills, TimeUnit.MILLISECONDS)
			.readTimeout(timeoutMills, TimeUnit.MILLISECONDS)
			.build();
		
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(baseURL)
			.addConverterFactory(ScalarsConverterFactory.create())
			.addConverterFactory(JacksonConverterFactory.create())
			.client(client)
			.build();
		
		return retrofit.create(clazz);
	}
	
	public static <T> Response<T> executeCall(Call<T> apiCall) {
		try {
			return apiCall.execute();
		}
		catch (IOException ex) {
			throw new RuntimeException("There was an exception making the API call: " + ex.getMessage());
		}
	}
	
	public static class Callback<T> implements retrofit2.Callback<T> {
		private boolean complete;
		private boolean successful;
		private Response<T> response;
		private Throwable exception;
		
		public Callback() {
			this.complete = false;
			this.successful = false;
			this.response = null;
			this.exception = null;
		}
		
		@Override
		public void onFailure(Call<T> call, Throwable t) {
			this.complete = true;
			this.successful = false;
			this.response = null;
			this.exception = t;
		}
		
		@Override
		public void onResponse(Call<T> call, Response<T> response) {
			this.complete = true;
			this.successful = response.isSuccessful();
			this.response = response;
			this.exception = null;
		}
		
		public boolean completed() {
			return this.complete;
		}
		
		public boolean wasSuccessful() {
			return completed() && this.successful;
		}
		
		public Response<T> getResponse() {
			return wasSuccessful() ? response : null;
		}
		
		public Throwable getException() {
			return wasSuccessful() ? null : exception;
		}
	}
}