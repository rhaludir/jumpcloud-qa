package com.jumpcloud.qa.common.interfaces;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.GET;
import retrofit2.http.Body;
import retrofit2.http.Path;

import com.jumpcloud.qa.common.models.*;

public interface HashserveInterface {
	
	@POST("/hash")
	public Call<Integer> postPassword(@Body HashPasswordBody body);
	
	@GET("/hash/{id}")
	public Call<String> getHash(@Path("id") int id);
	
	//---
	
	@GET("/stats")
	public Call<HashStatsResponse> getStats();
	
	//---
	
	@POST("/hash")
	public Call<Void> shutdown(@Body String body);
}