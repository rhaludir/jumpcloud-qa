# JumpCloud QA Assignment - Ryan George
---
## Structure

This project is divided into two man sections:  
  1. (main) common - The workflows and other structures commonly used across multiple test cases and suites.  
  2. test - The actual test methods and any supporting pre-test setup.
  
The rules and structures for interacting with the hashserve API are built out in the common library, including:  
  * Adapters - Defining the behavior of retrofit and instantiating the connection to the API itself.  
  * Interfaces - Defining the expected structure of the API including endpoints and message bodies.  
  * Models - Defining the structure of information passed to or received from the API.  
  * Helpers - Repeated tasks that were broken out from the tests or the common library.  

---
## Execution

To execute the tests, first ensure that the application being tested is running and accepting connections.

From the project directory (passwordHashTests), run the following command:  
  `mvn test -DserviceHostUrl={host} -DservicePort={port} clean test`  
  where `{host}` is the URL of the application (default: https://127.0.0.1)  
  and `{port}` is the port that the application is accepting connections on (default: 8088)
  
Test results are automatically printed to the terminal, or stored in `target/surefire-reports`.